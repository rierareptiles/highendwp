<?php
/**
 * Template part for displaying post format image entry.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package     Highend
 * @since       3.5.1
 */

/**
 * Do not allow direct script access.
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( post_password_required() ) { 
	return;
}

if ( ! has_post_thumbnail() ) {
	return;
}

$thumb  = get_post_thumbnail_id();
$crop   = hb_options( 'hb_blog_enable_image_cropping' );
$width  = 900;
$height = false;

if ( $crop ) {
	$height = is_single() ? hb_options( 'hb_blog_image_height', 500 ) : 500;
}

$image = highend_resize( $thumb, $width, $height, $crop );

if ( empty( $image ) || ! isset( $image['url'] ) || empty( $image['url'] ) ) {
	return; 
}

$srcset = $crop ? '' : ' srcset="' .  wp_get_attachment_image_srcset( $thumb, array( $width, $height ) ) . '"';

$anchor_atts = '';

if ( is_single() ) {
	$anchor_atts = 'data-title="' . esc_attr( get_the_title() ) . '" href="' . esc_url( wp_get_attachment_url( $thumb ) ) . '" rel="prettyPhoto"';
} else {
	$anchor_atts = 'href="' . esc_url( get_permalink() ) . '"';
}

?>
<div class="featured-image">
	<a <?php echo $anchor_atts; ?>>
		<img src="<?php echo esc_url( $image['url'] ); ?>" alt="<?php the_title(); ?>" width="<?php echo esc_attr( $image['width'] ); ?>" height="<?php echo esc_attr( $image['height'] ); ?>"<?php echo $srcset; ?>/>
		<div class="featured-overlay"></div>
		<div class="item-overlay-text">
			<div class="item-overlay-text-wrap">
				<span class="plus-sign"></span>
			</div>
		</div>
	</a>
</div>
