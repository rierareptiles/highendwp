jQuery( document ).ready( function() {
    jQuery("span.yith-auction-help-tip").each( function() {
        jQuery(this).html('?').removeClass('yith-auction-help-tip').addClass('custom_tooltipster');
    });
    jQuery('.custom_tooltipster').tooltipster({
        functionPosition: function(instance, helper, position){
            var height = jQuery( '#wpadminbar' ).css('height');
            position.coord.top -= parseInt( height ) || 0;
            return position;
        },
        maxWidth: 250,
        zIndex: 100000
    });
});