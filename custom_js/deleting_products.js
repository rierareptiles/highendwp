jQuery(document).ready( function() {
    jQuery('body').off('click', 'td.product-remove a').on( 'click', 'td.product-remove a', function(e) {
        e.preventDefault();
        jQuery( this ).closest('tr').fadeOut( "slow" );
        jQuery( this ).closest('tr').find('td.product-quantity input[type="number"], td.product-quantity input[type="hidden"]').val( '0' );
        jQuery( 'div.woocommerce input[name="update_cart"]').click();
        e.stopPropagation();
    });
    jQuery('body').off('click', '.woocommerce-remove-coupon').on( 'click', '.woocommerce-remove-coupon', function(e) {
        jQuery( this ).closest('li').fadeOut( "slow" );
        e.stopPropagation();
    });
});