<?php
/**
 * Common functions used in backend and frontend of the theme.
 *
 * @package Highend
 * @since   3.5.0
 */

/**
 * Do not allow direct script access.
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! function_exists( 'highend_get_social_networks_array' ) ) :

	/**
	 * Return array of available social networks.
	 * 
	 * @since 3.5.0
	 */
	function highend_get_social_networks_array() {

		$links = array(
			'envelop'     => 'Mail',
			'dribbble'    => 'Dribbble',
			'facebook'    => 'Facebook',
			'flickr'      => 'Flickr',
			'forrst'      => 'Forrst',
			'google-plus' => 'Google Plus',
			'html5'       => 'HTML 5',
			'cloud'       => 'iCloud',
			'lastfm'      => 'LastFM',
			'linkedin'    => 'LinkedIn',
			'paypal'      => 'PayPal',
			'pinterest'   => 'Pinterest',
			'reddit'      => 'Reddit',
			'feed-2'      => 'RSS',
			'skype'       => 'Skype',
			'stumbleupon' => 'StumbleUpon',
			'tumblr'      => 'Tumblr',
			'twitter'     => 'Twitter',
			'vimeo'       => 'Vimeo',
			'wordpress'   => 'WordPress',
			'yahoo'       => 'Yahoo',
			'youtube'     => 'YouTube',
			'github'      => 'Github',
			'yelp'        => 'Yelp',
			'mail'        => 'Mail',
			'instagram'   => 'Instagram',
			'foursquare'  => 'Foursquare',
			'xing'        => 'Xing',
			'vk'          => 'VKontakte',
			'behance'     => 'Behance',
			'twitch'      => 'Twitch',
			'sn500px'     => '500px',
			'weibo'       => 'Weibo',
			'tripadvisor' => 'Trip Advisor',
		);

		return apply_filters( 'highend_social_networks_array', $links );
	}
endif;

if ( ! function_exists( 'highend_is_module_enabled' ) ) :

	/**
	 * Check if a module is enabled.
	 * 
	 * @since 3.5.0
	 * @param string $module Module name
	 */
	function highend_is_module_enabled( $module ) {

		$enabled = true;

		if ( hb_options( 'hb_control_modules' ) && ! hb_options( $module ) ) {
			$enabled = false;
		}

		return $enabled;
	}
endif;

if ( ! function_exists( 'highend_hex2rgba' ) ) :
	/**
	 * Convert hexdec color string to rgb(a) string.
	 *
	 * @since  3.5.0
	 * @param  string           $color Hex color code.
	 * @param  string | boolean $opacity opacity value.
	 * @return string color in rgba format.
	 */
	function highend_hex2rgba( $color, $opacity = false ) {

		$default = 'rgb(0,0,0)';

		// Return default if no color provided.
		if ( empty( $color ) ) {
			return $default;
		}

		// Sanitize $color if "#" is provided.
		if ( '#' === $color[0] ) {
			$color = substr( $color, 1 );
		}

		// Check if color has 6 or 3 characters and get values.
		if ( 6 === strlen( $color ) ) {
			$hex = array( $color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5] );
		} elseif ( 3 === strlen( $color ) ) {
			$hex = array( $color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2] );
		} else {
			return $default;
		}

		// Convert hexadec to rgb.
		$rgb = array_map( 'hexdec', $hex );

		// Check if opacity is set(rgba or rgb).
		if ( $opacity ) {

			if ( abs( $opacity ) > 1 ) {
				$opacity = 1;
			}

			$output = 'rgba(' . implode( ',', $rgb ) . ',' . $opacity . ')';
		} else {
			$output = 'rgb(' . implode( ',', $rgb ) . ')';
		}

		// Return rgb(a) color string.
		return $output;
	}
endif;

if ( ! function_exists( 'highend_darken_color' ) ) :
	/**
	 * Adjust color brightness.
	 *
	 * @since  3.5.0
	 * @param  string $color Hex color code.
	 * @param  string $steps opacity value.
	 * @return string color in rgba format.
	 */
	function highend_darken_color( $hex, $steps ) {

		// Steps should be between -255 and 255. Negative = darker, positive = lighter.
		$steps = max( -255, min( 255, $steps ) );

		// Format the hex color string.
		$hex = str_replace( '#', '', $hex );
		if ( 3 === strlen( $hex ) ) {
			$hex = str_repeat( substr( $hex, 0, 1 ), 2 ) . str_repeat( substr( $hex, 1, 1 ), 2 ) . str_repeat( substr ( $hex, 2, 1 ), 2 );
		}

		// Get decimal values.
		$r = hexdec( substr( $hex, 0, 2 ) );
		$g = hexdec( substr( $hex, 2, 2 ) );
		$b = hexdec( substr( $hex, 4, 2 ) );

		// Adjust number of steps and keep it inside 0 to 255.
		$r = max( 0, min( 255, $r + $steps ) );
		$g = max( 0, min( 255, $g + $steps ) );  
		$b = max( 0, min( 255, $b + $steps ) );

		// Convert to hex.
		$r_hex = str_pad( dechex( $r ), 2, '0', STR_PAD_LEFT );
		$g_hex = str_pad( dechex( $g ), 2, '0', STR_PAD_LEFT );
		$b_hex = str_pad( dechex( $b ), 2, '0', STR_PAD_LEFT );

		return '#' . $r_hex . $g_hex . $b_hex;
	}
endif;

if ( ! function_exists( 'highend_display_notices' ) ) :
	/**
	 * Display notices.
	 *
	 * @deprecated 3.5.0
	 */
	function highend_display_notices() {
		return defined( 'WP_DEBUG_LOG' ) && WP_DEBUG_LOG || defined( 'WP_DEBUG' ) && WP_DEBUG;
	}
endif;

if ( ! function_exists( 'highend_is_maintenance' ) ) :
	/**
	 * Check if a maintenance mode is enabled.
	 * 
	 * @since 3.5.0
	 */
	function highend_is_maintenance() {

		$is_maintenance = false;

		if ( hb_options( 'hb_enable_maintenance' ) && ( ! is_user_logged_in() || ! current_user_can( 'edit_themes' ) ) ) {
			$is_maintenance = true;
		}

		return apply_filters( 'highend_is_maintenance', $is_maintenance );
	}
endif;

if ( ! function_exists( 'highend_get_the_id' ) ) :
	/**
	 * Get post ID.
	 *
	 * @since  3.6.2
	 * @return string Current post/page ID.
	 */
	function highend_get_the_id() {

		$post_id = 0;

		if ( 'page' === get_option( 'show_on_front' ) ) {
			if ( is_home() ) {
				$post_id = get_option( 'page_for_posts' );
			} elseif ( is_front_page() ) {
				$post_id = get_option( 'page_on_front' );
			}
		} elseif ( is_singular() ) {
			$post_id = get_the_ID();
		}

		return apply_filters( 'highend_get_the_id', $post_id );
	}
endif;

if ( ! function_exists( 'highend_get_post_meta' ) ) :
	/**
	 * Get metabox values.
	 *
	 * @since  3.6.2
	 * @param  string $key     Meta field ID.
	 * @param  mixed  $default Default value.
	 * @param  mixed  $post_id Post ID or object.
	 * @return mixed           Metabox value.
	 */
	function highend_get_post_meta( $key, $default = null, $post_id = null ) {
		
		if ( is_null( $post_id ) ) {
			$post_id = highend_get_the_id();
		}

		return vp_metabox( $key, $default, $post_id );
	}
endif;
